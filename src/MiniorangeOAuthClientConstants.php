<?php

namespace Drupal\oauth_login_oauth2;

/**
 * Handles constants used throughout the project.
 */
class MiniorangeOAuthClientConstants {

  const BASE_URL = 'https://login.xecurify.com';
  const SUPPORT_EMAIL = 'drupalsupport@xecurify.com';
  const FEEDBACK_URL = 'https://login.xecurify.com/moas/api/notify/send';

}
