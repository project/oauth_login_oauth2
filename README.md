# OAuth login module

This module lets users log in to your Drupal website using any OAuth 2.0 or OpenID Connect (OIDC) provider. We support a wide range of OAuth Providers, including Microsoft Entra AD, Keycloak, Azure AD B2C, Google Apps, GitHub, AWS Cognito, Salesforce, Discord, Ping Federate, and custom OAuth or OpenID providers.

The miniOrange OAuth / OIDC Client Login module facilitates authorization and authentication with any OAuth Provider/Server that adheres to the OAuth 2.0 and OpenID Connect (OIDC) 1.0 standards.

See: https://www.drupal.org/project/oauth_login_oauth2 for further information.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

NONE

## Recommended modules

https://www.drupal.org/project/usage/rest_api_authentication

https://www.drupal.org/project/miniorange_2fa

https://www.drupal.org/project/user_provisioning/

## Installation

See the detailed installation steps here :
https://www.drupal.org/docs/contributed-modules/drupal-oauth-oidc-login/how-to-install-oauth-oidc-sso-module

## Configuration

Follow the below setup guide:
https://www.drupal.org/docs/contributed-modules/drupal-oauth-oidc-login/drupal-oauth-login-setup-guide

## Maintainers

miniOrange Drupal team